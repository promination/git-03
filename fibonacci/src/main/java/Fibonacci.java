package main.java;

public class Fibonacci {

    public static void main(String[] args) {
        System.out.println(nthFibonacciTerm(Integer.parseInt(args[0])));
    }

    public static int nthFibonacciTerm(int n) {
        if (n == 1 || n == 0) {
            return n;
        }
        return nthFibonacciTerm(n-1) + nthFibonacciTerm(n-2);
    }
}
